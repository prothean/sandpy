======
Sandpy
======

Sandpy is your one stop shop for quickly spinning up a virtual environment for developing and debugging.

Usage
=====
Just call:

    `sandpy <project-name>`

**Currently only supported for linux.**
This will create your scrap virtual environment in the directory `$HOME/.sandpy/env/<project>` in *a new bash shell*.
